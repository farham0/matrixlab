﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project.Helpers
{
    public class QueryParametersPaging
    {
        const int _maxSize = 100;
        private int _size = 50;
        public int page { get; set; }
        public int size
        {
            get
            {
                return _size;
            }
            set
            {
                _size = Math.Min(_maxSize, Math.Abs(value));
            }
        }
    }
}
