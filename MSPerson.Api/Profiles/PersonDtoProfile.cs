﻿using AutoMapper;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.JsonPatch.Operations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Project.Domain;

namespace MSPerson.Api.DTOs
{
    public class PersonDtoProfile:Profile
    {
        public PersonDtoProfile()
        {
            // source -- destination 

            CreateMap<JsonPatchDocument<PersonUpdateDto>, JsonPatchDocument<Person>>();
            CreateMap<Operation<PersonUpdateDto>, Operation<Person>>();
            CreateMap<PersonUpdateDto, Person>();

            CreateMap<PersonCreateDto,Person>();
            CreateMap<Person,PersonReadDto>();
            CreateMap<PersonReadDto, Person>();
        }
    }
}
