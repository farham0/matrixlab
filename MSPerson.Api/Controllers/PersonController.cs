﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

using MSPerson.Api.Repository;
using MSPerson.Api.DTOs;
using AutoMapper;
using Project.Domain;

namespace MSPerson.Api.Controllers
{
    [ApiController]
    [Route("api/msp/[controller]")]
    public class PersonController : ControllerBase
    {

        private readonly ILogger<PersonController> _logger;
        private readonly PersonRepo _personRepository;
        private readonly IMapper _maper;

        public PersonController(ILogger<PersonController> logger, PersonRepo personRepository,IMapper maper)
        {
            _logger = logger;
            _personRepository = personRepository;
            _maper = maper;
        }

        [HttpGet("{id:int}")]
        public async Task<IActionResult> Get(int id)
        {
            if (id < 1)
            {
                return BadRequest();
            }

            var person = _personRepository.Get(id);
            if(person is null)
            {
                return NotFound();
            }

            return Ok(_maper.Map<PersonReadDto>(person));
        }

        [HttpPost]
        public  ActionResult<PersonReadDto> Post([FromBody] PersonCreateDto personCreateModel)
        {
            var personModel = _maper.Map<Person>(personCreateModel);

            try
            {
                _personRepository.Save(personModel);
                var personReadDto = _maper.Map<PersonReadDto>(personModel);
                return Ok (personReadDto);
            }
            catch
            {
                return BadRequest();
            }

        }

        [HttpPatch("{id:int}")]
        public async Task<IActionResult> Patch([FromRoute] int id, [FromBody] JsonPatchDocument<PersonUpdateDto>  personUpdateModel)
        {


            var personById = Get(id).Result;
            var notValidId = personById as BadRequestResult;
            var personFounded = personById as OkObjectResult;

            if (notValidId is not null)
            {
                return BadRequest();
            }

            if (personFounded is null)
            {
                return NotFound();
            }

            var currentPerson = new Person();
            _maper.Map(personFounded.Value,currentPerson);

            var updatePerosn = new PersonUpdateDto();
            personUpdateModel.ApplyTo(updatePerosn,ModelState);
            TryValidateModel(updatePerosn);


            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            
           // currentPerson.UpdateProperties(updatePerson);
            try
            {
               _personRepository.Save(_maper.Map(updatePerosn,currentPerson));
            }
            catch (Exception ex)// DbUpdateConcurrencyException need to be handled appropriately 
            {
                if(_personRepository.Get(id) is null)
                {
                    return NotFound();
                }
                this._logger.LogWarning(ex.Message);
                throw;
            }

            return NoContent();
        }


       
    }
}
