﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Project.Domain;

namespace MSPerson.Api.DTOs
{
    public class PersonCreateDto
    { 
        [Required]
        public string Name { get; set; }
        [Required]
        public DateTime DateBirth { get; set; }
        [Required]
        public GenderType Gender { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        public string Email { get; set; }

        public DateTime InsertTime { get; set; }
    }
}
