﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MSPerson.Api.DTOs
{
    public class PersonUpdateDto
    {

        [EmailAddress]
        public string Email { get; set; }
        [DataType(DataType.Text)]
        public string Name { get; set; }

    }
}
