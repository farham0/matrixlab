﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Project.Domain;

namespace MSPerson.Api.DTOs
{
    public class PersonReadDto
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public DateTime DateBirth { get; set; }

        public GenderType Gender { get; set; }

        public string Address { get; set; }

        public string Email { get; set; }

        public DateTime InsertTime { get; set; }
    }
}
