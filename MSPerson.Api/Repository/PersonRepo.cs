﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Project.Dal;
using Project.Domain;

namespace MSPerson.Api.Repository
{
    public class PersonRepo
    {

        private readonly IRepository<Person> _personRepository;
        public PersonRepo(IRepository<Person> personRepository)
        {
            this._personRepository = personRepository;
        }

        public Person Get(int id)
        {
            return _personRepository.Get(id);
        }

        internal void Save(Person currentPerson)
        {
            this._personRepository.Save(currentPerson);
        }
    }
}
