using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using Xunit;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using System;


using Project.Dal;
using Project.Domain;
using MSPerson.Api.Repository;
using MSPerson.Api.Controllers;
using AutoMapper;
using MSPerson.Api.DTOs;
using Project.Api.Test.Helpers;

namespace Project.Api.Test
{
    public class PersonControllerTest
    {
        private PersonRepo personRepository ;
        private readonly Mock<PersonRepository> _DominDalPersonRepo = new();

        private readonly Mock<ILogger<PersonController>> loggerMock = new();
        private readonly PersonHelper personHelper = new();
        private readonly IMapper mapper;

        public PersonControllerTest()
        {
            var mockMapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new PersonDtoProfile());
            });
            mapper = mockMapper.CreateMapper();
        }

        [Fact]            /// the things you test _ the value you sent _ the value you expect 
        public async void GetPersonById_WithUnexistingItem_ReturnsNotFound()
        {
            // a a a 


            _DominDalPersonRepo.Setup(repo => repo.Get(It.IsAny<int>()))
                .Returns((Person)null);
            personRepository = new PersonRepo(_DominDalPersonRepo.Object);


            var controller = new PersonController(loggerMock.Object, personRepository, mapper);

            var result = await controller.Get(It.IsAny<int>());

            //a
            result.Should().BeOfType<BadRequestResult>();
        }

        [Fact]
        public async void GetPersonById_WithexistingPerson_ReturnsExpectedPerson()
        {
            var expectedPerson = personHelper.CreateRandom();
            _DominDalPersonRepo.Setup(repo => repo.Get(It.IsAny<int>()))
                .Returns(expectedPerson);
            personRepository = new PersonRepo(_DominDalPersonRepo.Object);

            var controller = new PersonController(loggerMock.Object, personRepository, mapper);

            var result = await controller.Get(expectedPerson.Id) as ObjectResult;

            Assert.IsType<OkObjectResult>(result);
            result.Value.Should().BeEquivalentTo(expectedPerson,
                options => options.ComparingByMembers<Person>());
        }
       
    }
}
