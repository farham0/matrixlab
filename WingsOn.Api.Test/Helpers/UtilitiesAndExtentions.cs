﻿using System;
using System.Collections.Generic;

namespace Project.Api.Test.Helpers
{
    static class UtilitiesAndExtentions
    {
        private static Random rnd = new();

        public static List<T> CloneList<T>(this List<T> list)
        {
            return list.GetRange(0, list.Count);
        }

        public static IList<T> TakeRandom<T>(this IList<T> list, int number)
        {
            var TookedItem = new List<T>();
            for (var i = 0; i < number; i++)
            {
                var randomItm = list[rnd.Next(0, list.Count - 1)];
                if (!TookedItem.Contains(randomItm))
                {
                    TookedItem.Add(randomItm);
                }
            }

            return TookedItem;
        }
    }
}
