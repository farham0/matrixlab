﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Project.Domain;

namespace Project.Api.Test.Helpers
{
    class PersonHelper
    {

        CultureInfo cultureInfo = new CultureInfo("nl-NL");

        private Random rnd = new();
        
        public Person CreateRandom()
        {
            Array gender = Enum.GetValues(typeof(GenderType));

            return new Person
            {
                Id = rnd.Next(),
                Address = "805-1408 Mi Rd.",
                DateBirth = DateTime.Parse(
                    rnd.Next(1, 29).ToString() + "/" + rnd.Next(1, 12).ToString() + "/" + rnd.Next(1955, 1999).ToString(),
                    cultureInfo),
                Name = rnd.Next().ToString(),
                Gender = (GenderType)gender.GetValue(rnd.Next(gender.Length)),
                Email = "testMail" + rnd.Next() + "@gmail.com",
                InsertTime = DateTime.Now
            };
        }

        public List<Person> CreateListOfRandom()
        {
            return new List<Person>() {
                CreateRandom(),
                CreateRandom(),
                CreateRandom(),
                CreateRandom(),
                CreateRandom(),
            };
        }
    }
}
