import { Component, Inject, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from 'redux';
import { PersonRead } from 'src/app/Model/PersonRead';
import { appState } from 'src/app/redux/app.state';
import { AppStore } from 'src/app/redux/app.store';
import { httpPersonService } from '../../services/http/personServices';
import * as RecordActions from 'src/app/redux/userbehaviour.action'

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})

export class WelcomeComponent implements OnInit {
  acceptTermsCheckbox: boolean = false;
  public person!: PersonRead;
  public name!: string;
  constructor( @Inject(AppStore) private store:Store<appState>,
  private acvtiveRoute: Router, private personService: httpPersonService) { }

  ngOnInit(): void {
    this.personService.getPersonById(69).subscribe(
      data => { this.name = data.name }
    )
    this.store.dispatch(RecordActions.statRecording());
  }
  continue() {
    this.store.dispatch(RecordActions.recordAction("Accept Terms"));
    this.acvtiveRoute.navigate(["questions"]);
  }
}
