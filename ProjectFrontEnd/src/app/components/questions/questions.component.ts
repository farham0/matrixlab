import { Component, AfterViewInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { QuestionDomain } from 'src/app/Model/QuestionDomain';
import { QuestionBase } from 'src/app/Model/QuestionRender/QuestionBase';
import { httpQuestionsService } from 'src/app/services/http/questionsServices';
import { filter, map } from 'rxjs/operators';
import { QuestionRenderingServices } from 'src/app/services/rendering/questionRenderingServices';

import * as RecordActions from 'src/app/redux/userbehaviour.action'
import { appState } from 'src/app/redux/app.state';
import { AppStore } from 'src/app/redux/app.store';
import { Store } from 'redux';

@Component({
  selector: 'app-questions',
  templateUrl: './questions.component.html',
  styleUrls: ['./questions.component.css']
})
export class QuestionsComponent implements AfterViewInit {

  initialQuestions!: Array<QuestionBase<string>>;
  domainQuestions!: Array<QuestionBase<string>>;
  finalQuestions!: Array<QuestionBase<string>>;

  initFormReady: boolean = false;
  domainFormReady: boolean = false;
  finalFormReady: boolean = false;

  intialFormGroup!: FormGroup;
  domainFormGroup!: FormGroup;
  finalFormGroup!: FormGroup;

  summeryAnswers!: Array<any>;
  AnswerPayLoad!: any;

  constructor(
    @Inject(AppStore) private store:Store<appState>,
    private _formBuilder: FormBuilder, private acvtiveRoute: Router,
    private questionsService: httpQuestionsService, private questionRenderingServices: QuestionRenderingServices) {
    this.initialQuestions = [];
    this.finalQuestions = [];
    this.domainQuestions = [];
    this.finalFormGroup = this.questionRenderingServices.toFormGroup(this.finalQuestions);
  }

  ngAfterViewInit(): void {

    this.questionsService.getQuestionByType(QuestionDomain.intial)
      .pipe(
        map(c => c.map(c => this.questionsService.mapQuestionDtoToQuetsionBase(c))),
        filter(c => c != undefined),
      )
      .subscribe(data => {
        this.initialQuestions = [];
        data.forEach(q => this.initialQuestions.push(q as QuestionBase<string>));
        this.intialFormGroup = this.questionRenderingServices.toFormGroup(this.initialQuestions);
        this.intialFormGroup.controls["control3"].valueChanges.subscribe(c => { setTimeout(() => { this.DomainChange() }, 400); });
      }, null, () => { this.initFormReady = true });

    this.questionsService.getQuestionByType(QuestionDomain.Final)
      .pipe(
        map(data => data.map(q => this.questionsService.mapQuestionDtoToQuetsionBase(q))),
        filter(c => c != undefined)
      )
      .subscribe(data => {
        this.finalQuestions = [];
        data.forEach(q => this.finalQuestions.push(q as QuestionBase<string>));
        this.finalFormGroup = this.questionRenderingServices.toFormGroup(this.finalQuestions);
      }, null, () => { this.finalFormReady = true });

  }

  DomainChange() {
    let domainType = (this.intialFormGroup.value["control3"] == "Research management") ? "Research_management" : "Software_engineering";

    this.questionsService.getQuestionByType(domainType)
      .pipe(
        map(data => data.map(q => this.questionsService.mapQuestionDtoToQuetsionBase(q))),
        filter(c => c != undefined)
      )
      .subscribe(data => {
        this.domainQuestions = [];
        data.forEach(q => this.domainQuestions.push(q as QuestionBase<string>));
        this.domainQuestions = this.domainQuestions.slice();
        this.domainFormGroup = this.questionRenderingServices.toFormGroup(this.domainQuestions);
      }, null, () => { this.domainFormReady = true });
  }

  summery() {
    this.summeryAnswers = [
      ...this.initialQuestions.map(c => { let temp = { key: c.key, label: c.label, answer: null }; return temp }),
      ...this.domainQuestions.map(c => { let temp = { key: c.key, label: c.label, answer: null }; return temp }),
      ...this.finalQuestions.map(c => { let temp = { key: c.key, label: c.label, answer: null }; return temp })
    ]
    this.summeryAnswers.forEach(c => {
      let answer = null;
      answer = this.intialFormGroup.value[c.key];
      if (answer != null) { c.answer = answer; return }
      answer = this.domainFormGroup.value[c.key];
      if (answer != null) { c.answer = answer; return }
      answer = this.finalFormGroup.value[c.key];
      c.answer = answer;
    });
    this.store.dispatch(RecordActions.recordAction("Final Form Submit Button"));
  }

  onSubmit() {
    this.store.dispatch(RecordActions.endRecording());
    console.log("------------ Result --------------")
    console.log(this.summeryAnswers);
    console.log(this.store.getState());
    alert("Check the console for the Result");

  }

  recordUserAction(userAction:string){
    this.store.dispatch(RecordActions.recordAction(userAction));
  }


}


