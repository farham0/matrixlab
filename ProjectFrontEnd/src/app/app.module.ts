import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { BrowserModule } from '@angular/platform-browser';

import { HttpClientModule } from '@angular/common/http'

import { Routes, RouterModule } from '@angular/router'
import { MatToolbarModule } from '@angular/material/toolbar'
import { MatIconModule } from '@angular/material/icon'
import { MatButtonModule } from '@angular/material/button'
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatStepperModule } from '@angular/material/stepper';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field'; 
import {MatSelectModule } from '@angular/material/select'
import {MatDividerModule} from '@angular/material/divider';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { QuestionsComponent } from './components/questions/questions.component'; 
import { MatSidenavModule } from '@angular/material/sidenav';
import { DynamicFormQuestionComponent } from './components/questions/questionsRender/question';
import { Reporters } from './services/logs/Reporters';
import { logger1 } from './services/logs/logger1';
import { logger2} from './services/logs/logger2';

import {storeProvider} from './redux/app.store'

const RouteList: Routes = [
  { path: 'welcome', component: WelcomeComponent },
  { path: 'questions', component: QuestionsComponent }, 
  { path: '**', component: WelcomeComponent }];

@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    QuestionsComponent, 
    DynamicFormQuestionComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,

    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatSidenavModule,
    MatCheckboxModule,
    MatStepperModule,
    MatInputModule,
    MatFormFieldModule,
    MatSelectModule,
    MatDividerModule,

    RouterModule.forRoot(RouteList)
  ],
  providers: [{
    provide:Reporters,useClass:logger1,multi:true
  },
  {
    provide:Reporters,useClass:logger2,multi:true
  },storeProvider],
  bootstrap: [AppComponent]
})
export class AppModule { }
