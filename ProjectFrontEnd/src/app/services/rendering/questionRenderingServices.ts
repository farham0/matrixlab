import { Injectable } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { QuestionBase } from 'src/app/Model/QuestionRender/QuestionBase';

@Injectable({ providedIn: 'root' })
export class QuestionRenderingServices {
    toFormGroup(questions: QuestionBase<string>[]) {
        const group: any = {};
        questions.forEach(question => {
            group[question.key] =  new FormControl(question.value || '', Validators.required)
        });
        return new FormGroup(group);
    }
}