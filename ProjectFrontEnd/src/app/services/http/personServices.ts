import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { PersonRead } from 'src/app/Model/PersonRead';
import { baseServices } from './baseServices';
import { catchError, map, retry, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Reporters } from '../logs/Reporters';
import { IReport } from 'src/app/Model/IReport';

@Injectable({
    providedIn: 'root'
})
export class httpPersonService extends baseServices {
    private serviceBase = 'api/msp/Person/';
    constructor(private httpClient: HttpClient, @Inject(Reporters) private _reportsService:ReadonlyArray<IReport>) {
        super();
        this.reportServices=this._reportsService;
    }

    getPersonById(id: number): Observable<PersonRead> {
        this.logHttp("Read Person " + id);
        var url = this.baseEnv + this.serviceBase + id;
        return this.httpClient.get<PersonRead>(url)
            .pipe(
                tap(_ =>  console.log(_)),
                retry(3),
                catchError(this.handleError<PersonRead>(`Error:Get person id ${id}`))
            )

    }
}