import {Inject, Injectable} from '@angular/core'; 
import { HttpClient} from '@angular/common/http' 
import { environment } from "../../../environments/environment";
import { baseServices } from './baseServices';
import { QuestionDomain } from 'src/app/Model/QuestionDomain';
import { Observable } from 'rxjs';
import { QuestionReadDTO } from 'src/app/Model/QuestionRead';
import { catchError, retry, tap } from 'rxjs/operators';
import { QuestionBase } from 'src/app/Model/QuestionRender/QuestionBase';
import { QuestionTypes } from 'src/app/Model/QuestionTypes';
import { DropdownQuestion } from 'src/app/Model/QuestionRender/DropdownQuestion';
import { TextboxQuestion } from 'src/app/Model/QuestionRender/TextboxQuestion';
import { MultiSelectQuestion} from 'src/app/Model/QuestionRender/MultiSelectQuestion';
import { LabelQuestion} from 'src/app/Model/QuestionRender/LabelQuestion';
import { Reporters } from '../logs/Reporters';
import { IReport } from 'src/app/Model/IReport';

@Injectable({
    providedIn:'root'
})
export class httpQuestionsService extends baseServices{
    private serviceBase = 'api/msqu/questions/';
    constructor(private httpClient: HttpClient,@Inject(Reporters) private _reportsService:ReadonlyArray<IReport>) {
        super();
        this.reportServices=this._reportsService;
    }
    
    getQuestionByType(questionType:any):Observable<QuestionReadDTO[]>{   
        var url = this.baseEnv + this.serviceBase + questionType;
        this.logHttp("Read question "+questionType);
        return this.httpClient.get<QuestionReadDTO[]>(url)
        .pipe(
            retry(1),
            catchError(this.handleError<QuestionReadDTO[]>(`Get questions type ${questionType}`))
        )
        
    }
    mapQuestionDtoToQuetsionBase(obj: QuestionReadDTO) {
        switch (obj.questionType) {
          case QuestionTypes.SingleSelect:
            {
              return new DropdownQuestion({
                key:"control"+ obj.id.toString(),
                label: obj.question,
                options: obj.options.map<{ key: string, value: string }>(soption => { return { key: soption, value: soption } }),
              });
    
            }
          case QuestionTypes.Text: {
            return new TextboxQuestion({
              key:"control"+ obj.id.toString(),
              label: obj.question,
            });
          }
          case QuestionTypes.MultipleSelect:{
              return new MultiSelectQuestion({
                key:"control"+ obj.id.toString(),
                label: obj.question,
                options: obj.options.map<{ key: string, value: string }>(soption => { return { key: soption, value: soption } }),
              })
          }
          case QuestionTypes.Label:{
            return new LabelQuestion({
                key:"control"+ obj.id.toString(),
                label: obj.question
             })
          }
          default: {
            this.logHttp("Error: Type mismatch: "+ obj.questionType );
            return undefined;
          }
        }
    
      }
}