import { HttpClient } from "@angular/common/http";
import { Inject } from "@angular/core";
import { Observable, of } from "rxjs";
import { IReport } from "src/app/Model/IReport";
import { environment } from "../../../environments/environment";
import { Reporters } from "../logs/Reporters";

export abstract class baseServices {
    public baseEnv = environment.apiURL;
    public reportServices!:ReadonlyArray<IReport>;
    constructor() {
    }
    public handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // TODO: better job of transforming error for user consumption
            this.logHttp(`${operation} failed: ${error.message}`);

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }

    public logHttp(message:string){
        this.reportServices.forEach(rerportService => rerportService.send(message));
    }
}