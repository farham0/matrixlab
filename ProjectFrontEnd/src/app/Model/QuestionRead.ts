import { QuestionTypes } from "./QuestionTypes";

 

export interface QuestionReadDTO {
    id:number,
    question:string,
    questionType:QuestionTypes,
    options:Array<string>
}