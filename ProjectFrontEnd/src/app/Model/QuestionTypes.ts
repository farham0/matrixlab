export enum QuestionTypes {
    YesNo,
    SingleSelect,
    MultipleSelect,
    Text,
    Label
}