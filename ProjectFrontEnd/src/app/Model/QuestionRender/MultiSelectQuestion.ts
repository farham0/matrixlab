import { QuestionBase } from "./QuestionBase";


export class MultiSelectQuestion extends QuestionBase<string> {
    controlType = 'multiSelect';
  }