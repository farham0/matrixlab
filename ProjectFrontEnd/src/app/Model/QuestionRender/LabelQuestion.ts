import { QuestionBase } from "./QuestionBase";


export class LabelQuestion extends QuestionBase<string> {
    controlType = 'label';
  }