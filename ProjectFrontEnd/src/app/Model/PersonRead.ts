export interface PersonRead {
    id: number;
    name: string
    dateBirth: Date
    gender: string
    address: string
    email: string
    insertTime: Date
}