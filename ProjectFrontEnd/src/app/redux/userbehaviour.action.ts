import {BaseAction} from './baseAction'
import{Action,ActionCreator} from 'redux'
export const RECORDUSERINTERACTION="RECORDUSERINTERACTION";
export const STARTRECORDING ="STARTRECORDING";
export const ENDRECORDING="ENDRECORDING"


export const recordAction:ActionCreator<BaseAction>=(payload:string)=>({
    type:RECORDUSERINTERACTION,
    payload:payload
});
export const statRecording:ActionCreator<Action>=()=>({
    type:STARTRECORDING,
});
export const endRecording:ActionCreator<Action>=()=>({
    type:ENDRECORDING,
});
