import { Reducer, Action } from 'redux'
import { RECORDUSERINTERACTION, STARTRECORDING, ENDRECORDING } from './userbehaviour.action';
import { AppStore } from './app.store';
import { appState } from './app.state';
import { UserInteractions } from '../Model/UserInteraction';
import { BaseAction } from './baseAction';

const initialState: appState = {
    events: [],
    end: null,
    start: null,
}

export const reducer: Reducer<appState> = (state: appState = initialState, action: BaseAction) => {
    switch (action.type) {
        case STARTRECORDING: {
            return new Object({ events: [], start: new Date(), end: null }) as appState
        }
        case ENDRECORDING: {
            return new Object({
                events: [...state.events],
                end: new Date(),
                start: state.start
            }) as appState; 
        }
        case RECORDUSERINTERACTION: {
            return new Object({
                events: [...state.events,new Object({ date: new Date(), eventMessage: action.payload }) as UserInteractions],
                end: state.end,
                start: state.start
            }) as appState;
        } 
    } 
    return state;
}