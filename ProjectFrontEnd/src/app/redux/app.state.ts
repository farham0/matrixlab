import { UserInteractions} from '../Model/UserInteraction';
export interface appState{
    events:Array<UserInteractions>;
    start:Date|null;
    end:Date|null;
}