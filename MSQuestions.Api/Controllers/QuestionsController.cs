﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MSQuestions.Api.DTOs;
using MSQuestions.Api.Repository;
using Project.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MSQuestions.Api.Controllers
{
    [Route("api/msqu/[controller]")]
    [ApiController]
    public class QuestionsController : ControllerBase
    {
        private readonly ILogger<QuestionsController> _logger;
        private readonly QuestionsRepo _qersonRepository;
        private readonly IMapper _maper;

        public QuestionsController(ILogger<QuestionsController> logger, QuestionsRepo questionRepository, IMapper maper)
        {
            this._logger = logger;
            this._qersonRepository = questionRepository;
            this._maper = maper;
        }

        [HttpGet("{domain}")]
        public async Task<IActionResult> Get(QuestionDomain domain)
        {
            var GeneralQuestions = _qersonRepository.GetQuestionsByDomain(domain);
            return Ok(_maper.Map<IEnumerable<QuestionReadDto>>(GeneralQuestions));
        }
        
    }
}
