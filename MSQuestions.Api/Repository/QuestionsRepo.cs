﻿using Project.Dal;
using Project.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MSQuestions.Api.Repository
{
    public class QuestionsRepo
    {

        private readonly IRepository<Questions> _questionRepository;

        public QuestionsRepo(IRepository<Questions> repository)
        {
                _questionRepository = repository;
        }


        public IQueryable<Questions> GetQuestionsByDomain(QuestionDomain questionDomain) {
            return _questionRepository.GetAll().Where(q => q.Domain == questionDomain);
        }
    }
}
