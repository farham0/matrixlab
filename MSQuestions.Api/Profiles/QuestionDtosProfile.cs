﻿using AutoMapper;
using MSQuestions.Api.DTOs;
using Project.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace MSQuestions.Api.Profiles
{
    public class QuestionDtosProfile:Profile
    {
        public QuestionDtosProfile()
        {
            //map source -- destination
            CreateMap<Questions, QuestionReadDto>();
        }
    }
}
