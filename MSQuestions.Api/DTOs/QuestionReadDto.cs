﻿using Project.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MSQuestions.Api.DTOs
{
    public class QuestionReadDto
    {
        public int Id {  get; set; }

        public string Question { get; set; }

        public QuestionType QuestionType { get; set; }

        public List<string> Options { get; set; }
    }
}
