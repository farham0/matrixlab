﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project.Domain
{
    public class Answers : DomainObject
    {
        public Questions Question {  get; set; } 

        public Person Person {  get; set; }

        public string Respond { get; set; }
    }
}
