﻿using System;

namespace Project.Domain
{
    public class DomainObject
    {
        protected DomainObject()
        {
            this.InsertTime=DateTime.Now;
        }

        public DateTime InsertTime {  get; set;}
        public int Id { get; set; }
    }
}
