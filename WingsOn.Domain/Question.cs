﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project.Domain
{
    public class Questions : DomainObject
    {
        public string Question { get; set; }

        public QuestionDomain Domain { get; set; }

        public QuestionType QuestionType { get; set; }

        public List<string> Options { get; set; }
    }
}
