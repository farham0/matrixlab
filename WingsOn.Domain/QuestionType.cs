﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project.Domain
{
    public enum QuestionType
    {
        YesNo,
        SingleSelect,
        MultipleSelect,
        Text

    }
}
