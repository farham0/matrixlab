﻿using System;
using System.Linq;
using System.Reflection;

using WingsOn.Api.Model;
using WingsOn.Domain;

namespace WingsOn.Api.Helpers
{
    public static class ModelExtentions
    {
        public static T UpdateProperties<T, U>(this T target, U source) where T : DomainObject where U : UpdateModel
        {
            Type s = typeof(U);
            Type t = typeof(T);
            PropertyInfo[] sProps = s.GetProperties(BindingFlags.Public | BindingFlags.Instance);
            PropertyInfo[] tProps = t.GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo p in sProps)
            {
                if (!p.CanRead || !p.CanWrite) continue;

                object valSource = p.GetGetMethod().Invoke(source, null);
                if (valSource is null)
                {
                    continue;
                }

                var tProperty = tProps.Where(tp => tp.Name == p.Name).Single();
                if (tProperty is null)
                {
                    continue;
                }

                tProperty.SetValue(target, valSource);
            }

            return target;
        }
    }
}
