﻿using System.ComponentModel.DataAnnotations;
namespace WingsOn.Api.Model
{
    public class PersonUpdate: UpdateModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        public string Name {  get; set; } 
        
        [CreditCard]
        public string credCard { get; set; }

    }
}
