﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using WingsOn.Dal;
using WingsOn.Domain;

namespace WingsOn.Api.Repository
{
    public class FlightRepo        
    {

        private readonly IRepository<Booking> _bookingRepository;
        private readonly IRepository<Flight> _flightRepository;
        public FlightRepo(IRepository<Booking> bookingRepository, IRepository<Flight> flightRepository)
        {

            _bookingRepository = bookingRepository;
            _flightRepository = flightRepository;
        }
        public Flight GetById(int id)
        {
            return _flightRepository.GetAll()
               .Where(flight => flight.Id == id)
               .FirstOrDefault();
        }
        public Flight GetByNumber(string number)
        {
            return _flightRepository.GetAll()
               .Where(flight => flight.Number.ToLower() == number.ToLower())
               .FirstOrDefault();
        }

        public IEnumerable<Flight> GetAll()
        {
           return this._flightRepository.GetAll();
        }
    }
}
