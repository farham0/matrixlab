﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using WingsOn.Domain;
using WingsOn.Dal;

namespace WingsOn.Api.Repository
{
    public class BookingRepo
    {

        private readonly IRepository<Booking> _bookingRepository;
        public BookingRepo(IRepository<Booking> bookingRepository)
        {

            _bookingRepository = bookingRepository;
        }

        public IEnumerable<Person> getAllPassanger(Flight  flight)
        {
           return _bookingRepository.GetAll()
               .Where(booking => booking.Flight.Id == flight.Id)
               .SelectMany(booking => booking.Passengers);
        }

        public IEnumerable<Booking> GetAll()
        {
            return this._bookingRepository.GetAll();
        }
    }
}
