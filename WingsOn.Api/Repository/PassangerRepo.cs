﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using WingsOn.Dal;
using WingsOn.Domain;

namespace WingsOn.Api.Repository
{
    public class PassangerRepo
    {

        private readonly IRepository<Booking> _bookingRepository;
        public PassangerRepo(IRepository<Booking> bookingRepository)
        {
            this._bookingRepository= bookingRepository;
        }
        public IEnumerable<Person> getAllPassangerByGender(GenderType gebder ,int page , int size )
        {
            return  _bookingRepository.GetAll()
               .SelectMany(booking => booking.Passengers)
               .Where(person => person.Gender == gebder)
               .Skip((page - 1) * size)
               .Take(size);
        }
    }
}
