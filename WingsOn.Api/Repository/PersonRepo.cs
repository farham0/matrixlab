﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using WingsOn.Dal;
using WingsOn.Domain;

namespace WingsOn.Api.Repository
{
    public class PersonRepo
    {

        private readonly IRepository<Person> _personRepository;
        public PersonRepo(IRepository<Person> personRepository)
        {
            this._personRepository = personRepository;
        }

        public Person Get(int id)
        {
            return _personRepository.Get(id);
        }

        internal void Save(Person currentPerson)
        {
            this._personRepository.Save(currentPerson);
        }
    }
}
