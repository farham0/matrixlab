﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;


using WingsOn.Api.Model;
using WingsOn.Api.Helpers;
using WingsOn.Dal;
using WingsOn.Domain;
using WingsOn.Api.Repository;

namespace WingsOn.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PersonController : ControllerBase
    {

        private readonly ILogger<PersonController> _logger;
        private readonly PersonRepo _personRepository;

        public PersonController(ILogger<PersonController> logger, PersonRepo personRepository)
        {
            _logger = logger;
            this._personRepository = personRepository;
        }

        [HttpGet("{id:int}")]
        public async Task<IActionResult> Get(int id)
        {
            if (id < 1)
            {
                return BadRequest();
            }

            var person = _personRepository.Get(id);
            if(person is null)
            {
                return NotFound();
            }

            return Ok(person);
        }

        [HttpPatch("{id:int}/{email?}")]
        public async Task<IActionResult> Patch([FromRoute] int id, [FromBody] JsonPatchDocument<PersonUpdate>  personUpdateModel)
        {


            var personById = Get(id).Result;
            var notValidId = personById as BadRequestResult;
            var personFounded = personById as OkObjectResult;

            if (notValidId is not null)
            {
                return BadRequest();
            }

            if (personFounded is null)
            {
                return NotFound();
            }

            var currentPerson = personFounded.Value as Person;
            var updatePerson = new PersonUpdate();
            personUpdateModel.ApplyTo(updatePerson, ModelState);
            TryValidateModel(updatePerson);

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            
            currentPerson.UpdateProperties(updatePerson);
            try
            {
                _personRepository.Save(currentPerson);
            }
            catch (Exception ex)// DbUpdateConcurrencyException need to be handled appropriately 
            {
                if(_personRepository.Get(id) is null)
                {
                    return NotFound();
                }
                this._logger.LogWarning(ex.Message);
                throw;
            }

            return NoContent();

        }
       
    }
}
