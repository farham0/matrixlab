﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Threading.Tasks;

using WingsOn.Api.Helpers;
using WingsOn.Api.Model;
using WingsOn.Api.Repository;
using WingsOn.Dal;
using WingsOn.Domain;

namespace WingsOn.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PassengerController : ControllerBase
    {
        private readonly ILogger<PassengerController> _logger;
        private readonly PassangerRepo _passangerRepo;

        public PassengerController(ILogger<PassengerController> logger, PassangerRepo bookingRepository)
        {
            _logger = logger;
            _passangerRepo = bookingRepository;
        }

        [HttpGet("gender/{genderType}")]
        public async Task<IActionResult> Get(GenderType genderType, [FromQuery] QueryParametersPaging queryParametersPaging)
        {
            var passangersByGender = _passangerRepo
                .getAllPassangerByGender(genderType, queryParametersPaging.page, queryParametersPaging.size);
            if (passangersByGender is null)
            {
                return NotFound();
            }

            return Ok(passangersByGender);
        }

    }
}
