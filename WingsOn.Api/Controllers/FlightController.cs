﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Threading.Tasks;
using WingsOn.Api.Repository;
using WingsOn.Dal;
using WingsOn.Domain;

namespace WingsOn.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class FlightController : ControllerBase
    {
        private readonly ILogger<FlightController> _logger;
        private readonly FlightRepo _flightRepo;
        private readonly BookingRepo _bookingRepo;

        public FlightController(ILogger<FlightController> logger, FlightRepo flighRepo, BookingRepo bookingRepo)
        {
            _logger = logger;
            this._flightRepo = flighRepo;
            _bookingRepo = bookingRepo;
        }

        [HttpGet("{id:int}")]
        public async Task<IActionResult> Get(int id)
        {
            var flight = _flightRepo.GetById(id);
            if (flight is null)
            {
                return NotFound();
            }

            return Ok(flight);
        }

        [HttpGet("{number}")]
        public async Task<IActionResult> GetByNumber(string number)
        {
            var flight = _flightRepo.GetByNumber(number);
            if (flight is null)
            {
                return NotFound();
            }

            return Ok(flight);
        }


        [HttpGet("{number}/passengers")]
        public async Task<IActionResult> Get(string number)
        {
            var getFlightByNumber = GetByNumber(number).Result as OkObjectResult;
            if (getFlightByNumber is null)
            {
                return NotFound();
            }

            var flight = getFlightByNumber.Value as Flight;
            var passagners = _bookingRepo.getAllPassanger(flight);

            if (passagners is null)
            {
                return NotFound();
            }

            return Ok(passagners);
        }

    }
}
