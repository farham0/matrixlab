﻿using System.Collections.Generic;
using System.Linq;
using Project.Domain;

namespace Project.Dal
{
    public interface IRepository<T> where T : DomainObject
    {
        IQueryable<T> GetAll();

        T Get(int id);

        void Save(T element);
    }
}
