﻿using System;
using System.Collections.Generic;
using System.Text;
using Project.Domain;

namespace Project.Dal
{
    public class QuestionRepository : RepositoryBase<Questions>
    {
        public QuestionRepository()
        {
            Repository.AddRange(new[] {
                new Questions
                {
                    Id = 1,
                    Question ="Was it clear to you the interview time and manner (online or in-person)?",
                    QuestionType= QuestionType.SingleSelect,
                    Options= new List<string>() {"Yes","No" },
                    Domain=QuestionDomain.intial

                },
                new Questions
                {
                    Id = 2,
                    Question ="Was the person conducting the interview or discussion on time?",
                     QuestionType= QuestionType.SingleSelect,
                    Options=new List<string>() {"Yes","No" },
                    Domain=QuestionDomain.intial

                },
                new Questions
                {
                    Id = 3,
                    Question ="What post did you apply to?",
                    QuestionType= QuestionType.SingleSelect,
                    Options=new List<string>() { "Software engineering", "Research management" },
                   Domain=QuestionDomain.intial

                },
                new Questions
                {
                    Id = 4,
                    Question ="What topics were discussed during the interview?",
                    QuestionType= QuestionType.MultipleSelect,
                    Options=new List<string>() { "Frontend", "Backend", "Database", "CI/CD","Azure"},
                   Domain=QuestionDomain.Software_engineering

                },
                new Questions
                {
                    Id = 5,
                    Question ="What topics were discussed during the interview?",
                    QuestionType= QuestionType.MultipleSelect,
                    Options=new List<string>() { "Areas of research", "Data processing","Data analysis","People management","Time management" },
                   Domain=QuestionDomain.Research_management

                },
                new Questions
                {
                    Id = 6,
                    Question ="How can we improve our hiring process?",
                    QuestionType= QuestionType.Text,
                    Domain=QuestionDomain.Final

                }
            }); ;
        }
    }
}
