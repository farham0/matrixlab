﻿using System.Collections.Generic;
using System.Linq;
using Project.Domain;

namespace Project.Dal
{
    public class RepositoryBase<T> : IRepository<T> where T : DomainObject
    {
        protected List<T> Repository = new List<T>();

        protected RepositoryBase()
        {
        }

        public virtual IQueryable<T> GetAll() => Repository.AsQueryable();

        public virtual T Get(int id) => GetAll().SingleOrDefault(a => a.Id == id);

        public async void Save(T element)
        {
            if (element == null)
            {
                return;
            }

            T existing = Get(element.Id);
            if (existing != null)
            {
                Repository.Remove(existing);
            }

            Repository.Add(element);
        }
    }
}
